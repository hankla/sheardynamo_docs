\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,graphicx,caption, subcaption,wrapfig,siunitx}
\usepackage[hidelinks]{hyperref}
\usepackage{cleveref}
\usepackage{float,adjustbox,pdflscape}
\restylefloat*{figure}
\usepackage[numbers,sort&compress,longnamesfirst]{natbib}
\defcitealias{ssh16}{SSH16}
\defcitealias{gp15}{GP15}
\defcitealias{nb15}{NB15}
\newcommand{\ipath}{/home/hankla/analysis_sheardynamo/figures}



\title{Dependence of the Nonhelical Dynamo on Shear:\\Summary}
\author{Lia Hankla\\Advisor: Christian Fendt}
\date{May 1, 2018}

\begin{document}
\maketitle

\section{Introduction}
This project seeks to study the influence of shear on an unstratified dynamo. Previous studies such as~\citet{gp15}, henceforth referred to as~\citetalias{gp15}, have studied a range of shearing parameters in the case of stratified dynamos, where the alpha-effect is likely to be in action. In light of recent papers, e.g.~\citet{sb15} etc., there is renewed interest in the potential of other mechanisms, present even in the absence of stratification/helicity, to generate and maintain a mean magnetic field. There seem to be two main competing theories for nonhelical dynamo generation: the incoherent alpha effect explored by e.g.~\citet{mb12}, and the recently-proposed magnetic shear-current effect~\cite{rk04, sb15}. Can we distinguish which of these models better captures the nonhelical physics by examining the dependence of dynamo coefficients on shear? \\
\\
This work so far combines elements of~\citet{ssh16}, from now on referred to as~\citetalias{ssh16}, to determine if the dynamo is in action based on ratios of turbulent and mean magnetic energy and azimuthal magnetic energy, extended over a range of shears. The fact that~\citetalias{ssh16} works with unstratified, zero net flux magnetic fields allows for a direct comparison of Keplerian shear $q=15$. Using this data point and the work of~\citetalias{gp15} which studies stratified (small net vertical flux) magnetic fields over shear allows a check on the methods of the present study.

\section{Past Work Comparison/Extension}
\subsection{Presence of Dynamo}
Sometimes it can be hard to tell from the butterfly diagram if the dynamo is actually present/periodic or not. Here we use~\citetalias{ssh16}'s criteria of large-scale ($\approx$ size of $H$, the scale height) magnetic fields as well as the ratio of mean and turbulent total and azimuthal magnetic energy. We also use their conventions of $\langle\rangle$ volume-average and $B=\bar B+b$. In Section 3.2,~\citetalias{ssh16} mentions that the large-scale dynamo maintains a time-averaged mean field $\langle\langle \bar B^2/8\pi P_0\rangle\rangle_t$ that is approximately 40-50\% of the magnetic energy in the box, that is, the turbulent and mean field energy is nearly equal, as opposed to when the dynamo is not present, when the mean field is weaker and drops to $\lessapprox 10$\%. Another indicator is the percentage of the saturated Maxwell stress that is in the mean vs. turbulent field. In a small box where the dynamo is not active, the stress is almost entirely in the turbulent field, whereas in larger boxes the mean field stress accounts for $\approx16\%$ of the stress. Section 3.3 also indicates that in smaller boxes, the magnetic energy is mostly due to the turbulent azimuthal magnetic energy $\langle B^2\rangle\sim\langle b_y^2\rangle\gg\langle\bar B_y^2\rangle$ as opposed to the mean azimuthal magnetic energy $\langle B^2\rangle\sim\langle\bar B^2_y\rangle\gg\langle b^2_y\rangle$ when the dynamo is in action. To this end, they calculate the mean and turbulent magnetic alpha
\begin{align*}
  \alpha_{mag}^{(m)} = -2\frac{\langle\bar B_x\bar B_y\rangle}{\langle\bar B^2\rangle}; \quad \alpha_{mag}^{(t)}=-2\frac{\langle b_xb_y\rangle}{\langle B^2\rangle-\langle\bar B^2\rangle}
\end{align*}
A comparison of the proportion of magnetic energy in the mean field $\langle\langle\bar B^2\rangle\rangle_t/\langle\langle B^2\rangle\rangle_t$, the ratio of Maxwell stress in the mean vs. turbulent field $\langle\langle \bar B_x\bar B_y/ b_xb_y\rangle\rangle_t$, and other ratios are presented in Table~\ref{tab:sshvals}. Their dependence on the shearing parameter $q$ is explored in~\Cref{fig:MEtot,fig:MEmean,fig:MEmtr,fig:MEymtr,fig:maturb,fig:mamean}. \\
\\
Table~\ref{tab:sshvals} shows several similar trends to~\citetalias{ssh16}. For instance, column ``MEymtr'' shows the mean field energy is approximately the same proportion of the total energy as the turbulent field in large boxes and only about 10\% in small boxes where the dynamo is suppressed. The proportion increases with decreasing resolution. We also see that the ratio of mean to total Maxwell stress as shown in column ``MXmTr'' increases dramatically in tall boxes, from 2.7\% to 16\% for $z=1$ and $z=4$, respectively. \\
\\
As Fig.~\ref{fig:MEtot} shows, we recover both the trend of increasing magnetic energy with decreasing resolution in a small box and increased magnetic energy relative to the small box in tall boxes as found in~\citetalias{ssh16}. A curious part of this figure is how abruptly the magnetic energy dies off. A first interpretation suggests that the dynamo can only operate for $q\ge1.2$, below which the magnetic energy is just as low as in small boxes. The same trend is evident in the mean magnetic energy, shown in Fig.~\ref{fig:MEmean}, which also shows that the mean magnetic energy does not depend on resolution in a small box where the dynamo cannot generate a mean magnetic field. The ratio of mean to turbulent magnetic energy as shown in Fig.~\ref{fig:MEmtr} shows that indeed, approximately half of the magnetic energy is in the mean field while the dynamo is active, as compared to only about 10\% when it is not. The mean azimuthal magnetic energy shows a similar pattern seen in Fig.~\ref{fig:MEymtr}, approximately constant above/below a critical value of $q\approx1.2$. The mean Maxwell stress has a wider spread, with a higher proportion for $32/H$ resolution, but is otherwise similar (also not shown). \\
\\
Also consistent with~\citetalias{ssh16}, the turbulent magnetic alpha is roughly constant with resolution, as shown in Fig.~\ref{fig:maturb}. This number is also roughly constant with shear; note the small range in values. Similarly, we recover the spread in mean magnetic alpha values in resolution, with smaller resolution having higher values as shown in Fig.~\ref{fig:mamean}. For a tall box however, the mean magnetic alpha is roughly constant, possibly slightly decreasing with decreasing shear. How can we reconcile this with the sharp cutoffs in magnetic energy?


%\begin{landscape}
    \input{\ipath/tab_vata_mtT.txt}
%\end{landscape}

\begin{figure}
  \centering
  \begin{minipage}{.48\textwidth}
    \centering
    \includegraphics[width=\linewidth]{\ipath/vata_mtT/fig_vata_mtT_MEtot.pdf}
    \caption{Averaged from $t=50$ orbits.}
    \label{fig:MEtot}
  \end{minipage}%
  \hfill
 \begin{minipage}{.48\textwidth}
  \centering
  \includegraphics[width=\linewidth]{\ipath/vata_mtT/fig_vata_mtT_MEmean.pdf}
  \caption{Averaged from $t=50$ orbits.}
  \label{fig:MEmean}
 \end{minipage}
\end{figure}

\begin{figure}
  \centering
  \begin{minipage}{.48\textwidth}
    \centering
    \includegraphics[width=.9\textwidth]{\ipath/vata_mtT/fig_vata_mtT_MEmtr.pdf}
    \caption{Averaged from $t=50$ orbits.}
    \label{fig:MEmtr}
  \end{minipage}
  \hfill
  \begin{minipage}{.48\textwidth}
    \centering
    \includegraphics[width=.9\textwidth]{\ipath/vata_mtT/fig_vata_mtT_MEymtr.pdf}
    \caption{Averaged from $t=50$ orbits.}
    \label{fig:MEymtr}
  \end{minipage}
\end{figure}



\begin{figure}
  \centering
  \begin{minipage}{.48\textwidth}
    \centering
    \includegraphics[width=\linewidth]{\ipath/vata_mtT/fig_vata_mtT_maturb.pdf}
    \captionof{figure}{Averaged from $t=50$ orbits.}
    \label{fig:maturb}
  \end{minipage}%
  \hfill
  \begin{minipage}{.48\textwidth}
    \centering
    \includegraphics[width=\linewidth]{\ipath/vata_mtT/fig_vata_mtT_mamean.pdf}
    \caption{Averaged from $t=50$ orbits.}
    \label{fig:mamean}
  \end{minipage}
\end{figure}

\subsection{Scaling Relations}
Past studies of the shear parameter have established several scaling laws dependent on $q$~\cite{abl96,Pessah06}. Figs.~\ref{fig:mxrx} and~\ref{fig:tx} show the two most popular; that is, a scaling of total stress as $q~(2-q)^{-1}$ and the ratio of Maxwell-to-Reynolds stress as $(4-q)~q^{-1}$. The values are averaged from $t=50$ orbits to the end of the simulations, usually around $t=200$ orbits. The error bars mark one standard-deviation from the mean.

\begin{figure}
  \centering
  \begin{minipage}{.48\textwidth}
    \centering
    \includegraphics[width=\linewidth]{\ipath/vata_mxrx_ssh7/fig_vata_mxrx_ssh7_log_fit.pdf}
    \caption{Averaged from $t=50$ orbits.}
    \label{fig:mxrx}
  \end{minipage}%
  \hfill
  \begin{minipage}{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{\ipath/vata_tx_gp1/fig_vata_tx_gp1_log_fit.pdf}
    \caption{Averaged from $t=50$ orbits.}
    \label{fig:tx}
  \end{minipage}
\end{figure}


%\section{Figure Comparison}
\subsection{Butterfly Diagram}
As in~\citetalias{ssh16}, we find large-scale magnetic fields form in tall ($z=4$) boxes but not in small ($z=1$) boxes, see Fig.~\ref{fig:bzcomp}. With the added dimension of varying the shear as in~\citetalias{gp15} Fig. 7 or~\citetalias{nb15} Fig. 6, as shown in Fig.~\ref{fig:z4q691215}, we see large-scale structures even at $q<1.2$, even though the values computed in Tab.~\ref{tab:sshvals} and portrayed in~\Cref{fig:MEtot,fig:MEmean,fig:MEmtr,fig:MEymtr,fig:maturb,fig:mamean} would indicate that no dynamo is present. 


\begin{figure}
  \centering
  \includegraphics[width=.9\linewidth]{\ipath/xyte_by/fig_xyte_by_hgbLAIF_r64b4000q15_z1z4.pdf}
  \caption{Difference in average azimuthal magnetic field for small (top) and tall (bottom) boxes. The black dotted box indicates the actual simulation domain.}
  \label{fig:bzcomp}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=.9\linewidth]{\ipath/xyte_by/fig_xyte_by_hgbLAIF_r64z4b4000_q06q09q12q15.pdf}
  \caption{Large-scale structure in averaged azimuthal magnetic field for shear parameters of $q=0.6$, 0.9, 1.2, and 1.5. Note that the limits of the colorbars change for each panel.}
  \label{fig:z4q691215}
\end{figure}



\section{Next Steps/Possible Directions}
\begin{itemize}
    \item Calculate the dynamo coefficients
    \item Determine predictions for magnetic shear/incoherent alpha
    \item Determine how to distinguish magnetic shear/incoherent alpha
    \item Linear statistical method with compressible flows?
    \item Plot growth rate of $B_{rms}$ as per~\citet{sb15} Fig. 4. 
\end{itemize}

\section{Specific Questions}
\begin{itemize}
  \item Periodic boundary conditions artificial?
  \item How know if dynamo? (computed ratios, but butterfly cycles still there...)
  \item How distinguish MRI vs dynamo growth?
  \item Raedler vs shear-current effect as mechanism to generate $\eta_{yx}$ interesting?
  \item Impact of numerical Re on ideal simulations?
  \item Impact of compressibility?
  \item Why is the shear-current effect controversial?
  \item Interplay of MRI being stopped by too strong a magnetic field vs quenching of dynamo? Turbulence gone --> no dynamo? i.e. dynamo at $\beta\ll1$
    \item Magnetic flux vs energy generated?

\end{itemize}



\newpage
\bibliographystyle{unsrtnat}
\bibliography{../bibTeX/sheardynamo} 
\end{document}
